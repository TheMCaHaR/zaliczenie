package bazar;

import Baza.impl.DummyRolaRepository;
import dane.*;

public interface IKatalog {
	public IPracownikRepo getPracownicy();
	public IPobierz<Osoba> getOsoby();
	public DummyRolaRepository getRole();
	public void commit();
}
