package bazar;
import java.util.List;

import dane.*;
public interface IPracownikRepo extends IPobierz <Pracownik> {
	public List<Pracownik> withPensja(Pensja pensja);
	
	public List<Pracownik> withPensja(int pensjaId);
}
