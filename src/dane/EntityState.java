package dane;

public enum EntityState {
	New, Changed, Unchanged, Deleted
}
