package dane;

import java.util.*;

import dane.Osoba;
import dane.Rola;


public class Pracownik extends Entity{

		private List<Pensja> pensje;
	private Osoba osoba;
	private List<Rola> role;
	
	public Pracownik()
	{
		role=new ArrayList<Rola>();
	}
	
	public Osoba getOsoba() {
		return osoba;
	}
	
	public List<Pensja> getPensje() {
		return pensje;
	}

	
	public void setPensja(List<Pensja> string) {
		this.pensje = string;
	}



	
	public List<Rola> getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(List<Rola> role) {
		this.role = role;
	}
	public void setOsoba(Osoba osoba) {
		this.osoba = osoba;
		if(!this.equals(osoba.getPracownik()))
			osoba.setPracownik(this);
	}

	}