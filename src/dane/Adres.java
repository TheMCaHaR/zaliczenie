package dane;

public class Adres {

	private String kraj;
	private String miasto;
	private String kodPocztowy;
	private String ulica;
	private String numer;
	/**
	 * @return the kraj
	 */
	public String getKraj() {
		return kraj;
	}
	/**
	 * @param kraj the kraj to set
	 */
	public void setKraj(String kraj) {
		this.kraj = kraj;
	}
	/**
	 * @return the miasto
	 */
	public String getMiasto() {
		return miasto;
	}
	/**
	 * @param miasto the miasto to set
	 */
	public void setMiasto(String miasto) {
		this.miasto = miasto;
	}
	/**
	 * @return the kodPocztowy
	 */
	public String getKodPocztowy() {
		return kodPocztowy;
	}
	/**
	 * @param kodPocztowy the kodPocztowy to set
	 */
	public void setKodPocztowy(String kodPocztowy) {
		this.kodPocztowy = kodPocztowy;
	}
	/**
	 * @return the ulica
	 */
	public String getUlica() {
		return ulica;
	}
	/**
	 * @param ulica the ulica to set
	 */
	public void setUlica(String ulica) {
		this.ulica = ulica;
	}
	/**
	 * @return the numer
	 */
	public String getNumer() {
		return numer;
	}
	/**
	 * @param numer the numer to set
	 */
	public void setNumer(String numer) {
		this.numer = numer;
	}
	/**
	 * @return the osoba
	 */
	public Osoba getOsoba() {
		return osoba;
	}
	/**
	 * @param osoba the osoba to set
	 */
	public void setOsoba(Osoba osoba) {
		this.osoba = osoba;
	}
	private Osoba osoba;
	
}