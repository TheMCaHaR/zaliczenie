package dane;

import java.util.*;

public class Uprawnienia extends Entity {
	
	private String nazwa;
	private List<Rola> rola;
		
	public Uprawnienia()
	{
		rola = new ArrayList<Rola>();
	}

	/**
	 * @return the nazwa
	 */
	public String getNazwa() {
		return nazwa;
	}

	/**
	 * @param nazwa the nazwa to set
	 */
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	/**
	 * @return the rola
	 */
	public List<Rola> getRola() {
		return rola;
	}

	/**
	 * @param rola the rola to set
	 */
	public void setRola(List<Rola> rola) {
		this.rola = rola;
	}

	
}