package dane;

public class Pensja extends Entity {
	private int pensja;

	/**
	 * @return the pensja
	 */
	public int getPensja() {
		return pensja;
	}

	/**
	 * @param pensja the pensja to set
	 */
	public void setPensja(int pensja) {
		this.pensja = pensja;
	}
	
}
