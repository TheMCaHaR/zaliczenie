package dane;



import java.util.ArrayList;
import java.util.List;

public class Rola extends Entity {
	
	private String name;
	private List<Uprawnienia> uprawnienia;
	private List<Pracownik> pracownik;
	
	public Rola()
	{
		uprawnienia = new ArrayList<Uprawnienia>();
		pracownik = new ArrayList<Pracownik>();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the uprawnienia
	 */
	public List<Uprawnienia> getUprawnienia() {
		return uprawnienia;
	}

	/**
	 * @param uprawnienia the uprawnienia to set
	 */
	public void setUprawnienia(List<Uprawnienia> uprawnienia) {
		this.uprawnienia = uprawnienia;
	}

	/**
	 * @return the pracownik
	 */
	public List<Pracownik> getPracownik() {
		return pracownik;
	}

	/**
	 * @param pracownik the pracownik to set
	 */
	public void setPracownik(List<Pracownik> pracownik) {
		this.pracownik = pracownik;
	}

	

}