package dane;
import java.util.*;

public class Osoba extends Entity{

	public Osoba(){
		
		this.adresy=new ArrayList<Adres>();
	}
	private int id;
	private String Imie;
	private String Nazwisko;
	private String PESEL;
	
	private Pracownik pracownik;
	
	private List<Adres> adresy;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the imie
	 */
	public String getImie() {
		return Imie;
	}

	/**
	 * @param imie the imie to set
	 */
	public void setImie(String imie) {
		Imie = imie;
	}

	/**
	 * @return the nazwisko
	 */
	public String getNazwisko() {
		return Nazwisko;
	}

	/**
	 * @param nazwisko the nazwisko to set
	 */
	public void setNazwisko(String nazwisko) {
		Nazwisko = nazwisko;
	}

	/**
	 * @return the pESEL
	 */
	public String getPESEL() {
		return PESEL;
	}

	/**
	 * @param pESEL the pESEL to set
	 */
	public void setPESEL(String pESEL) {
		PESEL = pESEL;
	}

	/**
	 * @return the pracownik
	 */
	public Pracownik getPracownik() {
		return pracownik;
	}

	/**
	 * @param pracownik the pracownik to set
	 */
	public void setPracownik(Pracownik pracownik) {
		this.pracownik = pracownik;
	}

	/**
	 * @return the adresy
	 */
	public List<Adres> getAdresy() {
		return adresy;
	}

	/**
	 * @param adresy the adresy to set
	 */
	public void setAdresy(List<Adres> adresy) {
		this.adresy = adresy;
	}
	
}
