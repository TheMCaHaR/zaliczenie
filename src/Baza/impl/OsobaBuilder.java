package Baza.impl;
import java.sql.ResultSet;
import java.sql.SQLException;
import dane.Osoba;

public class OsobaBuilder implements TEntityBuilder<Osoba> {

	@Override
	public Osoba build(ResultSet rs) throws SQLException {
		Osoba osoba = new Osoba();
		osoba.setId(rs.getInt("id"));
		osoba.setImie(rs.getString("imie"));
		osoba.setNazwisko(rs.getString("nazwisko"));
		osoba.setPESEL(rs.getString("pesel"));
		return osoba;
	}
}
