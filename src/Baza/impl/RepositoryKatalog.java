package Baza.impl;


import java.sql.Connection;

import dane.Osoba;
import dane.Rola;
import bazar.*;
import unitofwork.IUnitOfWork;

public class RepositoryKatalog implements IKatalog{

	private Connection connection;
	private IUnitOfWork uow;
	
	public RepositoryKatalog(Connection connection, IUnitOfWork uow) {
		super();
		this.connection = connection;
		this.uow = uow;
	}
	
	@Override
	public IPracownikRepo getPracownicy() {
			return new PracownikRepository(connection, new PracownikBuilder(), uow);
	}
	
	@Override
	public IPobierz<Osoba> getOsoby() {
			return new OsobaRepository(connection, new OsobaBuilder(), uow);
	}
	
	@Override
	public DummyRolaRepository getRole() {
			return null;
	}
	@Override
	public void commit() {
		uow.commit();
	
	}
}