package Baza.impl;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dane.Osoba;
import bazar.IKatalog;
import unitofwork.IUnitOfWork;

public class OsobaRepository extends Repository<Osoba>{
	
	protected OsobaRepository(Connection connection,
				TEntityBuilder<Osoba> builder, IUnitOfWork uow) {
			super(connection, builder, uow);
			}

	
	@Override
	protected void setUpInsertQuery(Osoba entity) throws SQLException {
		insert.setString(1, entity.getImie());
		insert.setString(2, entity.getNazwisko());
		insert.setString(3, entity.getPESEL());
	}
	@Override
	protected void setUpUpdateQuery(Osoba entity) throws SQLException {
		update.setString(1, entity.getImie());
		update.setString(2, entity.getNazwisko());
		update.setString(3, entity.getPESEL());
		update.setInt(4, entity.getId());
	}
	
	@Override
	protected String getTableName() {
			return "osoba";
	}
	
	@Override
	protected String getUpdateQuery() {
		return "update osoba set (imie,nazwisko,pesel)=(?,?,?)"
				+ "where id=?";
	}
	
	@Override
	protected String getInsertQuery() {
		return "insert into osoba(imie,nazwisko,pesel) values(?,?,?)";
	}


}
