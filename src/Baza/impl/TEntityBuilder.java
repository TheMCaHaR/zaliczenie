package Baza.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import dane.Entity;

public interface TEntityBuilder<TEntity extends Entity> {
	
	public TEntity build(ResultSet rs) throws SQLException;
}
