package Baza.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import bazar.IKatalog;
import unitofwork.IUnitOfWork;
import unitofwork.UnitOfWork;

public class RepositoryKatalogProvider {


	private static String url="jdbc:hsqldb:hsql://localhost/workdb";
	
	public static IKatalog catalog()
	{

		try {
			Connection connection = DriverManager.getConnection(url);
			IUnitOfWork uow = new UnitOfWork(connection);
			IKatalog catalog = new RepositoryKatalog(connection, uow);
		
			return catalog;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}