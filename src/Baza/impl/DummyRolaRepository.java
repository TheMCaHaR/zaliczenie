package Baza.impl;
import java.util.List;

import bazar.IPobierz;
import dane.Rola;

public class DummyRolaRepository implements IPobierz<Rola> {
private DummyDb db;
	
	public DummyRolaRepository(DummyDb db) {
				 super();
				 this.db = db;
	}
	
	@Override
	public void save(Rola entity) {
			db.role.add(entity);
	}
	
	@Override
	public void update(Rola entity) {
	}
	
	@Override
	public void delete(Rola entity) {
			db.role.remove(entity);
	}
	@Override
	public Rola get(int id) {
		for(Rola rola : db.role)
			if(rola.getId()==id)
				return rola;
	return null;
	}
	
	@Override
	public List<Rola> getAll() {
			return db.role;
	}
	
}
