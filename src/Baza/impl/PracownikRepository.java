package Baza.impl;

import java.sql.*;
import java.util.List;

import bazar.IPracownikRepo;
import unitofwork.IUnitOfWork;
import dane.Pensja;
import dane.Osoba;
import dane.Rola;
import dane.Pracownik;

public class PracownikRepository
	extends Repository<Pracownik> implements IPracownikRepo{
	
	public PracownikRepository(Connection connection,
			TEntityBuilder<Pracownik> builder, IUnitOfWork uow) {
		super(connection,builder, uow);
	}
	

	@Override
	protected String getTableName() {
		return "pracownicy";
	}
	
	@Override
	protected String getUpdateQuery() {
		return
				"UPDATE pracownicy SET (pensje)=(?) WHERE id=?";
	}
	
	@Override
	protected String getInsertQuery() {
		return "INSERT INTO pracownicy(pensje)"
				+ "VALUES(?)";
	
	}

	@Override
	public List<Pracownik> withPensja(Pensja pensja) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Pracownik> withPensja(int pensjaId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	protected void setUpUpdateQuery(Pracownik entity) throws SQLException {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected void setUpInsertQuery(Pracownik entity) throws SQLException {
		// TODO Auto-generated method stub
		
	}

}