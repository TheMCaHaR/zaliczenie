package Baza.impl;
import java.util.List;

import dane.Pensja;
import dane.Pracownik;
import bazar.IPracownikRepo;
import bazar.IKatalog;

public class DummyPracownikRepository implements IPracownikRepo {
private DummyDb db;
	
	public  DummyPracownikRepository(DummyDb db) {
			super();
			this.db = db;
		}
	@Override
	public void save(Pracownik entity) {
			db.pracownicy.add(entity);
		
	}

	@Override
	public void update(Pracownik entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Pracownik entity) {
			db.pracownicy.remove(entity);
		
	}

	@Override
		public Pracownik get(int id) {
				for(Pracownik u: db.pracownicy)
						if(u.getId()==id)
							return u;
			return null;
	}

	@Override
	public List<Pracownik> getAll() {
			return db.pracownicy;
	}

	@Override
	public List<Pracownik> withPensja(Pensja pensja) {
			return withPensja(Pensja.getId());
	}

	@Override
	public List<Pracownik> withPensja(int pensjaId) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
