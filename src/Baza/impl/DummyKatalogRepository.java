package Baza.impl;
import bazar.*;
import dane.*;
public class DummyKatalogRepository implements IKatalog {
	DummyDb db = new DummyDb();

	@Override
	public IPracownikRepo getPracownicy() {
		
		return new DummyPracownikRepository(db);
}
	@Override
	public IPobierz<Osoba> getOsoby() {
	
			return new DummyOsobaRepository(db);
	}
	@Override
	public DummyRolaRepository getRole() {
	
		 return new DummyRolaRepository(db);
	}
	@Override
	public void commit() {
		// TODO Auto-generated method stub
		
	}

}
