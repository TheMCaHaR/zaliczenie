package Baza.impl;
import java.util.List;

import bazar.IKatalog;
import bazar.IPobierz;
import dane.Osoba;
public class DummyOsobaRepository implements IPobierz<Osoba> {
		
	private DummyDb db;
	
	public DummyOsobaRepository(DummyDb db) {
		super();
		this.db = db;
	}
	@Override
	public void save(Osoba osoba) {
			db.osoby.add(osoba);
	}
	
	@Override
	public void update(Osoba osoba) {
			// TODO Auto-generated method stub
	}
	
	@Override
	public void delete(Osoba osoba) {
			db.osoby.remove(osoba);
	}
	
	@Override
	public Osoba get(int id) {
		for(Osoba person : db.osoby)
			if(person.getId()==id)
				return person;
		return null;
	}
	
	@Override
	public List<Osoba> getAll() {
			return db.osoby;
	}
	
}
