package Baza.impl;


import java.sql.ResultSet;
import java.sql.SQLException;

import dane.Pracownik;

public class PracownikBuilder implements TEntityBuilder<Pracownik> {
	
	@Override
	public Pracownik build(ResultSet rs) throws SQLException {
		Pracownik pracownik = new Pracownik();
		pracownik.setId(rs.getInt("id"));
		pracownik.setPensja(rs.getString("Pensja"));
		return pracownik;
	
	}

}